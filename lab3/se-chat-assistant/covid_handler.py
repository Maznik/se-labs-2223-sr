from query_handler_base import QueryHandlerBase
import random
import requests
import json

class CovidHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "covid" in query:
            return True
        return False

    def process(self, query):       
        try:
            result = self.call_api()
            country = result["country"]
            continent = result["continent"]
            TotalCases = result["total cases"]
            self.ui.say(f"Country: {country}\nContinent: {continent}\nTotal Cases: {TotalCases}")
        except: 
            self.ui.say("Oh no! There was an error trying to contact Covid api.")
            self.ui.say("Try something else!")



    def call_api(self):
        url = "https://vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com/api/npm-covid-data/world"

        headers = {
            "X-RapidAPI-Key": "ce3f275c84mshc5f1d36259adebcp13a5d1jsnea70936dbd0e",
            "X-RapidAPI-Host": "vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com"
        }

        response = requests.request("GET", url, headers=headers)

        return json.loads(response.text)
