while 1:
    p1 = input("Igrac broj jedan: ")
    p2 = input("Igrac broj dva: ")
    if p1 == p2:
        print("Nerijeseno.")
    elif p1 == 'rock':
        if p2 == 'scissors':
            print("Rock wins! Igrac broj jedan je pobjedio")
        else:
            print("Paper wins!")
    elif p1 == 'scissors':
        if p2 == 'paper':
            print("Scissors win!")
        else:
            print("Rock wins!")
    elif p1 == 'paper':
        if p2 == 'rock':
            print("Paper wins!")
        else:
            print("Scissors win!")
    x = input("Ukoliko zelite nastaviti igru upisite 'da' u suprotnom 'ne': ")
    if x == "ne":
        break