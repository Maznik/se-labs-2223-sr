def occurences(numbers, number):
    occurencesInList = 0
    for x in numbers:
        if number == x:
            occurencesInList += 1
    return occurencesInList

def comma_sep_to_int(values):
    numbers = []
    for number in values.split(","):
        if(number != ""):
            numbers.append(int(number))
    return numbers

def centered_average(numbers):
    l_min = min(numbers)
    l_max = max(numbers)
    filteredList = []
    for x in numbers:
        if (x != l_min and x != l_max):
            filteredList.append(x)
        else:
            if occurences(numbers,x)>1 and x not in filteredList:
                filteredList.append(x)

numbers = comma_sep_to_int(input("Input coma separated: "))
print(centered_average(numbers))