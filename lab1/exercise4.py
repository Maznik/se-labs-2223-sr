def function(string):
    uppCount = sum(1 for c in string if c.isupper())
    lowCount = sum(1 for c in string if c.islower())
    numCount = sum(1 for c in string if c.isnumeric())
    return(uppCount, lowCount, numCount)

string = input("Input string: ")
print(function(string))